CXX ?= g++

DEBUG ?= 1
ifeq ($(DEBUG), 1)
    CXXFLAGS += -g
else
    CXXFLAGS += -O2

endif

all: tcp_server udp_server

tcp_server: main.cpp  ./src/mysql_server.cpp ./src/tcp_net.cpp ./src/tcp_kernel.cpp ./src/thread_pool.cpp ./src/udp_net.cpp
	$(CXX) -o tcp_server  $^ $(CXXFLAGS) -lpthread -lmysqlclient

clean:
	rm  -r tcp_server
	rm udp_server

udp_server: udp_server.cpp
	g++ udp_server.cpp -std=c++11 -lpthread -o udp_server
